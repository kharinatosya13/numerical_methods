import random
from traceback import format_tb
import numpy as np
import matplotlib.pyplot as plt


class system:

    f1 = lambda x, y: np.sin(2*x - y) - 1.2*x - 0.4
    dxf1 = lambda x, y: 2*np.cos(2*x - y) - 1.2
    dyf1 = lambda x, y: -np.cos(2*x - y)

    f2 = lambda x, y: 0.8*x**2 + 1.5*y**2 - 1
    dxf2 = lambda x, y: 1.6*x
    dyf2 = lambda x, y: 3*y


class Calculator:

    check = "https://www.geogebra.org/calculator/dy6kfakk"

    def __init__(self, eps, system):
        self.system = system
        self.e = eps

    def jacobian(self, f, x, y):
        return np.array([[f.dxf1(x, y), f.dyf1(x, y)], [f.dxf2(x, y), f.dyf2(x, y)]])

    def newton(self, x0, y0):
        x = x0
        y = y0
        i = 0
        while True:
            i+=1
            A = self.jacobian(self.system, x, y)
            F = np.array([self.system.f1(x, y), self.system.f2(x, y)])
            delta = np.linalg.solve(A, F)
            x -= delta[0]
            y -= delta[1]
            if np.linalg.norm(delta) <= self.e:
                return x, y

    def draw(self, a, b):

        x = np.linspace(a, b, 100)
        y = np.linspace(a, b, 100)
        X, Y = np.meshgrid(x, y)
        Z1 = self.system.f1(X, Y)
        Z2 = self.system.f2(X, Y)
        plt.contour(X, Y, Z1, [0], colors='red')
        plt.contour(X, Y, Z2, [0], colors='blue')
        plt.grid()
        plt.show()

if __name__ == '__main__':
    calc = Calculator(0.0001, system)
    print(calc.newton(-5, -5))
    print(calc.newton(5, 5))
    print(calc.newton(-0.6, -0.3))
    print(calc.newton(1.0, 1.0))
    calc.draw(-5,5)

