from cmath import sin
import math
import struct
from time import sleep
from xml.etree.ElementTree import PI
import numpy as np
import matplotlib.pyplot as plt
import threading

class function:
    def __init__(self):
        self.a = 3.0
        self.b = 1.0
        
    f = lambda self, x: self.a*x + np.cos(x) + self.b
    df = lambda self, x: self.a - np.sin(x)
    ddf = lambda self, x: -np.cos(x)
    phi = lambda self, x: (-np.cos(x)-self.b)/self.a
    dphi = lambda self, x: np.sin(x)/self.a 

class result:
    def __init__(self, x, i, apr = 0):
        self.x = x
        self.i = i
        self.apr = apr
   
class Calculator:
    def __init__(self, e=0.0001, segment = (-3,0), func = function()):
        self.e = e
        self.segment = segment
        self.func = func
        self.real_x = np.linspace(self.segment[0], self.segment[1], 100)
        self.real_y = self.func.f(self.real_x)
        self.der_y = self.func.df(self.real_x)
        self.dder_y = self.func.ddf(self.real_x)
        self.phi_y = self.func.phi(self.real_x)
        self.der_phi_y = self.func.dphi(self.real_x)

    def newton_method(self):
        x = -1
        apr_t = abs(x - self.segment[0]) if  abs(x - self.segment[0]) > abs(x - self.segment[1]) else abs(x - self.segment[1])
        q = (self.dder_y.max()*apr_t)/(2.0*self.der_y.min())
        apr = math.log((np.log(apr_t/self.e)/(np.log(1.0/q)) + 1), 2) + 1
        x_prev = 0
        i = 0
        while abs(x_prev - x) > self.e:
            x_prev = x
            x -= self.func.f(x)/self.func.df(x)
            i += 1
        plt.scatter(x, 0, color='orange', s=40, marker='o')
        return result(x, i, apr)

    def iteration_method(self):
        x = -1
        q = abs(self.der_phi_y).max()
        apr = np.log(abs(self.func.phi(x)-x)/((1.0-q)*self.e))/np.log(1.0/q) + 1
        x_prev = 0
        i = 0
        cond = ((1.0-q)/q)*self.e if q < 1.0/2.0 else self.e
        while abs(x_prev - x) > cond:
            x_prev = x
            x = self.func.phi(x)
            i += 1
        plt.scatter(x, 0, color='orange', s=40, marker='o')
        return result(x, i, apr)

    def draw(self):
        plt.plot(self.real_x, self.real_y, label='f(x)')
        plt.plot(self.real_x, self.der_y, label='df(x)')
        if len(self.phi_y) > 0:
            plt.plot(self.real_x, self.phi_y, label='phi(x)')
        plt.grid(True)
        plt.show()

def print_result(result):
    print("Результат: " + str(result.x) + "\tКількость кроків: " + str(result.i) + "\tАпріорна оцінка: " + str(result.apr))


if __name__ == '__main__':
    calculator = Calculator()
    print("Метод Ньютона: ")
    print_result(calculator.newton_method())
    print("Метод простої ітерації: ")
    print_result(calculator.iteration_method())
    calculator.draw()
