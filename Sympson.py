import numpy as np
import matplotlib.pyplot as plt
 
f_x = lambda x: 1 / (9-x)
 
def sympson(left, right, n, function):
    h = (right - left) / (2 * n)
    print("Step: ", h)
    result = float(function(left)) + float(function(right))
    for step in range(1, 2 * n):
        if step % 2 != 0:
            result += 4 * float(function(left + step * h))
        else:
            result += 2 * float(function(left + step * h))

    return result * h / 3

def draw_plot(a, b, function):
    x = np.linspace(a, b, 100)
    y = function(x)
    plt.plot(x, y)
    plt.grid()
    plt.show()

real_result = 1.09861228
sympson_result = sympson(3, 7, 1, f_x)
print("f(x) = 1 / (9-x)")
print("(a, b) = (3, 7), n = 1")
print(f"👨 Sympson: {sympson_result}")
print(f"Real result: log(3) ≈ {real_result}...")
print(f"Accuracy = 0.05; Real accuracy = {abs(sympson_result - real_result)}")
if abs(sympson_result - real_result) < 0.05:
    print("👍 Result is correct!")
else:
    print("👎 Result is incorrect!")
draw_plot(3, 7, f_x)